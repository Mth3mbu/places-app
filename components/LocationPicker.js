import React, { useState, useEffect } from 'react';
import * as Location from 'expo-location';

import {
  View,
  Text,
  Button,
  ActivityIndicator,
  Alert,
  StyleSheet,
} from 'react-native';

import Colors from '../constants/Colors';
import MapPreview from '../components/MapPreview';

const LocatonPicker = (props) => {
  const [pickedLocation, setLocation] = useState();
  const [isFetching, setFetching] = useState(false);
  const mapPickedLocation = props.navigation.getParam('pickedLocation');
  const { onLocationPicked } = props;
  useEffect(() => {
    if (mapPickedLocation) {
      setLocation(mapPickedLocation);
      onLocationPicked(mapPickedLocation);
    }
  }, [mapPickedLocation, onLocationPicked]);

  const getPermision = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
      Alert(
        'Insufficient permisions!',
        'You need to grant Location permissions to use this app',
        [{ text: 'Okay' }]
      );
      return false;
    }
    return true;
  };

  const pickOnMapHandler = () => {
    props.navigation.navigate('Map');
  };

  const getLocationHandler = async () => {
    const hasPermission = await getPermision();
    if (hasPermission) {
      try {
        setFetching(true);
        const location = await Location.getCurrentPositionAsync({
          timeInterval: 5000,
        });

        setLocation({
          lat: location.coords.latitude,
          lon: location.coords.longitude,
        });

        onLocationPicked({
          lat: location.coords.latitude,
          lon: location.coords.longitude,
        });
      } catch (ex) {
        Alert.alert(
          'Could not fetch location',
          'Please make sure you are connected to the internet',
          [{ text: 'OK' }]
        );
      }
    }
    setFetching(false);
  };

  return (
    <View style={styles.locationPicker}>
      <MapPreview location={pickedLocation} onPress={pickOnMapHandler}>
        {isFetching ? (
          <ActivityIndicator size='large' color={Colors.primary} />
        ) : (
          <Text>Please pick a Loacation</Text>
        )}
      </MapPreview>

      <View style={styles.actionButtonsContainer}>
        <Button
          title='Current Location'
          color={Colors.primary}
          onPress={getLocationHandler}
        />
        <Button
          title='Pick on Map'
          color={Colors.primary}
          onPress={pickOnMapHandler}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  locationPicker: { marginBottom: 15 },

  actionButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
});

export default LocatonPicker;
