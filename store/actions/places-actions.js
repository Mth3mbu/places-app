import * as FileSystem from 'expo-file-system';
import { insertPlace, fetchtPlaces } from '../../helpers/db';
import ENV from '../../env';
export const ADD_PLACE = 'ADD_PLACE';
export const SET_PLACES = 'SET_PLACES';

const getAddress = async (location) => {
  try {
    const response = await fetch(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${location.lat},${location.lng}&key=${ENV.googleAPIKey}`
    );

    if (!response.ok) {
      throw new Error('Something went wrong while fetching the address');
    }

    return await response.json();
  } catch (ex) {
    console.log(ex);
  }
  return null;
};

export const addPlace = (title, image, location) => {
  return async (dispatch) => {
    const fileName = image.split('/').pop();
    const newPath = `${FileSystem.documentDirectory}${fileName}`;
    const resData = await getAddress(location);
    const address = resData.results[0].formatted_address;

    console.log(address);
    try {
      await FileSystem.moveAsync({
        from: image,
        to: newPath,
      });

      const dbResults = await insertPlace(
        title,
        newPath,
        address,
        location.lat,
        location.lng
      );

      dispatch({
        type: ADD_PLACE,
        placeData: {
          id: dbResults.insertId,
          title: title,
          image: newPath,
          address: address,
          coords: { lat: location.lat, lng: location.lng },
        },
      });
    } catch (e) {
      console.log(e);
    }
  };
};

export const loadPlaces = () => {
  return async (dispatch) => {
    try {
      const dbResults = await fetchtPlaces();

      dispatch({ type: SET_PLACES, places: dbResults.rows._array });
    } catch (e) {
      console.log(e);
    }
  };
};
